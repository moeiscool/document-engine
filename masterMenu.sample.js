function buildBasicBreadCrumb(endName,parents){
    let headerTitle = []
    const breadcrumbs = [
        { label: "Home", href: '/'},
        ...parents,
        { label: endName },
    ];
    parents.forEach((crumb) => {
        headerTitle.push(crumb.label);
    });
    return {
        headerTitle: headerTitle.join(' > '),
        breadcrumbs
    }
}
function addBreadCrumbs(items,parents){
    items.forEach((item,n) => {
        const {
            headerTitle,
            breadcrumbs,
        } = buildBasicBreadCrumb(item.label, parents)
        items[n].headerTitle = headerTitle;
        items[n].breadcrumbs = breadcrumbs;
    })
    return items
}
module.exports = {
    items: [
        { label: "Home", href: '/', isRemote: true },
        {
            label: "Installation",
            href: '/installation',
            items: addBreadCrumbs([
                { label: "The Ninja Way", href: '/installation/ninja-way'},
                { label: "Docker", href: '/installation/docker' },
                { label: "Raspberry Pi", href: '/installation/raspberry-pi', hubArticle: 'dRqw1Cqnty81Aii', theme: {main: '#730222', linkColor: '#e4265b'}},
                { label: "Jetson Nano", href: '/installation/jetson-nano', hubArticle: 'i1bbMtvqC2NO1B2', theme: {main: '#1a873b', linkColor: '#20ce55'}},
            ],[
                { label: "Installation", href: '/installation'}
            ])
        },
        {
            label: "Post-Installation",
            href: '/configure',
            itemDraw: true,
            items: addBreadCrumbs([
              {
                "label": "First Login",
                "href": "/configure/first-login",
                "hubArticle": "eTglRKk00AdV1Xz",
                "description": "You will need to login to the Superuser and create an Admin account. Then login to the Admin account from the Dashboard login page."
              },
              {
                "label": "Adding a Camera",
                "href": "/configure/add-monitor-configuration",
                "hubArticle": "kBKrD0mWQ0D4Yar",
                "description": "You can use the built-in ONVIF Scanner to quickly add camera to Shinobi. Sometimes it's not that easy, in which case you can attempt to add it Manually."
              },
              {
                "label": "Adding Limited Accounts",
                "href": "/configure/add-limited-accounts",
                "hubArticle": "hUY26tR7zCSBZjk",
                "description": "You can create additional accounts under your Admin account from the Sub-Account Manager. You can limit their access to certain cameras or data sets."

              },
              {
                "label": "Activation",
                "href": "/configure/activate",
                "hubArticle": "2RxeJETY2eamKny",
                "description": "Subscribing to a plan with Shinobi offers additional benefits like Easy Remote Access and Private Monitor Configuration backup to ShinobiHub."

              },
              {
                "label": "Remote Management",
                "href": "/remote",
                "description": "Access your Shinobi anywhere in the world where there is internet using our in-house P2P servers. You can also use your own VPN or Port-Forward."
              },
            ],[
                { label: "Post-Installation", href: '/configure'}
            ])
        },
        {
            label: "Community and Support",
            href: '/community-and-support',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "News", href: 'https://shinobi.video/news', isRemote: true, newTab: true },
                { label: "Contributing", href: '/contributing'},
                { label: "Community Chat", href: 'https://shinobi.community/', isRemote: true, newTab: true },
                { label: "Forum on Reddit", href: 'https://reddit.com/r/ShinobiCCTV', isRemote: true, newTab: true },
                { label: "Merge Requests", href: 'https://gitlab.com/Shinobi-Systems/Shinobi/-/merge_requests', isRemote: true, newTab: true },
                { label: "Issues on Gitlab", href: 'https://gitlab.com/Shinobi-Systems/Shinobi/-/issues', isRemote: true, newTab: true },
            ],[
                { label: "Notifications", href: '/notifications'}
            ])
        },
        {
            label: "Monitors and Cameras",
            href: '/monitors',
            itemDraw: true,
            items: addBreadCrumbs([
                {
                  "label": "Adding a Camera",
                  "href": "/configure/add-monitor-configuration",
                  "hubArticle": "kBKrD0mWQ0D4Yar",
                },
                { label: "Monitor Logs", href: '/system/logs', hubArticle: 'bp4CWlGcXNHHHAL' },
                { label: "Troubleshoot a Camera", href: '/configure/troubleshoot-camera', hubArticle: 'v0AFPFchfVcFGUS' },
                { label: "Scheduling Monitors", href: '/configure/monitor-configuration-schedule', hubArticle: '6ylYHj9MemlZwrM' },
                { label: "How to setup PTZ", href: '/configure/ptz-setup', hubArticle: 'eRNcAjM95fhEaZL' },
                { label: "PTZ Tracking", href: '/configure/ptz-tracking', hubArticle: 'Fvl1QhTaX7d6tcU' },
                { label: "Included ONVIF Device Manager", href: '/configure/onvif-device-manager', hubArticle: 'pERwszQ5cIz6gXq' },
                { label: "Additional Input Feeds", href: '/configure/additional-input-feeds', hubArticle: 'w8azEAI2peYeNul' },
                { label: "Dynamic Substream", href: '/configure/dyanmic-substream', hubArticle: 'xm9HJFXI1XITt1y' },
            ],[
                { label: "Monitors and Cameras", href: '/monitors'}
            ])
        },
        {
            label: "Notifications",
            href: '/notifications',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Email", href: '/notifications/email', hubArticle: 'XG8LUcWtsN4Teq1' },
                { label: "Discord", href: '/notifications/discord', hubArticle: 'Vx8KOsrUXTZxsSp' },
                { label: "Telegram", href: '/notifications/telegram', hubArticle: '6vz3lYGHXb2Cqra' },
            ],[
                { label: "Notifications", href: '/notifications'}
            ])
        },
        {
            label: "Detection and Events",
            href: '/detect',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Motion Detection", href: '/detect/motion', hubArticle: '6Y4SkeQSKk6PMvc' },
                { label: "Object Detection", href: '/detect/object', hubArticle: 'nI8wPEScbCbGPfa' },
                { label: "ONVIF Event Triggering", href: '/detect/onvif-events', hubArticle: 'FT2LAfGk5VrMHZC' },
                { label: "FTP-based Triggering", href: '/detect/ftp-events', hubArticle: 'LyCI3yQsUTouSAJ' },
                { label: "SMTP-based Triggering", href: '/detect/smtp-events', hubArticle: 'Qdu39Dp8zDqWIA0' },
                { label: "MQTT Triggering", href: '/detect/mqtt', hubArticle: 'LXw2fdkTPIU8BlP' },
                // { label: "Requirements", href: '/object/requirements' },
                { label: "Detector Plugins", href: '/create/detector-plugin', hubArticle: 'NCdcizUXqLzp1H8' },
                { label: "Tensorflow (Object)", href: '/detect/tensorflow-js', gitReadme: 'plugins/tensorflow' },
                { label: "Tensorflow Coral (Object)", href: '/detect/tensorflow-coral-js', gitReadme: 'plugins/tensorflow-coral' },
                { label: "Yolo (Object)", href: '/detect/yolo', gitReadme: 'plugins/yolo' },
                { label: "Deepstack (Object)", href: '/detect/deepstack', hubArticle: 'PcBtEgGuWuEL529' },
                { label: "Facial Recognition (Object)", href: '/detect/face', gitReadme: 'plugins/face' },
                // { label: "OpenALPR", href: '/object/openalpr' },
                // { label: "PlateRecognizer", href: '/object/platerecognizer' },
                { label: "Event Filtering", href: '/detect/event-filters', hubArticle: 'Z7q0tvrIV3IGc40' },
                { label: "Detector Plugins in Cluster Mode", href: '/configure/plugins-cluster-mode', hubArticle: 'IH7ZFpl2a2NIcmf' },
                { label: "Trigger Multiple Cameras", href: '/configure/monitor-event-multi-trigger', hubArticle: 'NYfAa2RqRRCmIen' },
                { label: "Motion Detection (v2)", href: '/detect/motion-v2', hubArticle: 'LKdcgcgWy9RJfUh', hidden: '/detect/motion' },
            ],[
                { label: "Detection", href: '/detect'}
            ])
        },
        {
            label: "Backup and Migration",
            href: '/backup-migration',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Migrate your Shinobi", href: '/migrate/shinobi-server', hubArticle: 'SUjjJEQEsP2B0IW' },
                { label: "Migrate your ZoneMinder", href: '/migrate/zoneminder', hubArticle: 'vXTPnYNvh4sZRtW' },
                { label: "Backup Monitors", href: '/backup/monitor-configurations', hubArticle: 'QzWPj4vp8Y2k1R5' },
                { label: "Automatically Backup Monitors", href: '/backup/private-monitor-configurations-to-shinobihub', hubArticle: 'spOpdcnPrCpZMYo' },
                { label: "Backup to Amazon S3", href: '/backup/amazon-s3-cloud-storage', hubArticle: 'p55gs40VJradtHN' },
                { label: "Backup to Backblaze B2", href: '/backup/backblaze-b2-cloud-storage', hubArticle: 'S5De8Kbvi7cLSdG' },
                { label: "Backup to Google Drive", href: '/backup/google-drive-cloud-storage', hubArticle: 'oEIDEGXLlzmSsCk' },
            ],[
                { label: "Backup and Migration", href: '/backup-migration'}
            ])
        },
        {
            label: "API",
            href: '/api',
            items: addBreadCrumbs([
                { label: "Authentication", href: '/api/authentication', oldDoc: `api-authentication`, theme: { main: '#333',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Managing API Keys by UI", href: '/api/managing-api-keys-ui', hubArticle: 'or1kfTcwpo7UivO' },
                { label: "Managing API Keys by API", href: '/api/managing-api-keys-api', oldDoc: `api-managing-api-keys`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Get Monitors", href: '/api/get-monitors', oldDoc: `api-getting-monitors`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Get Streams", href: '/api/get-streams', oldDoc: `api-get-streams`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Embedding Streams", href: '/api/embedding-streams', oldDoc: `api-embedding-streams`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Get Videos", href: '/api/get-videos', oldDoc: `api-getting-videos`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
//
                { label: "Add, Edit or Delete a Monitor", href: '/api/add-edit-or-delete-a-monitor', oldDoc: `api-add-edit-or-delete-a-monitor`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Modifying a Video or Deleting it", href: '/api/modifying-a-video-or-deleting-it', oldDoc: `api-modifying-a-video-or-deleting-it`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Monitor Triggers", href: '/api/monitor-triggers', oldDoc: `api-monitor-triggers`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
//
                { label: "Superuser", href: '/api/superuser-only', oldDoc: `api-superuser-only`, theme: { main: '#333',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Administrator", href: '/api/administrator-only', oldDoc: `api-administrator-only`, theme: { main: '#333',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Monitor Presets", href: '/api/monitor-states-preset-configurations', oldDoc: `api-monitor-states-preset-configurations`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "Schedules for Monitor Presets", href: '/api/scheduling-for-monitors', oldDoc: `api-scheduling-for-monitors`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "System", href: '/api/system-triggers', oldDoc: `api-system-triggers`, theme: { main: '#027355',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
                { label: "ONVIF Management through Shinobi", href: '/api/direct-camera-management-via-onvif', oldDoc: `api-direct-camera-management-via-onvif`, theme: { main: '#305374',textColorOnMain: '#ffffff',hyperlinkColorOnMain: '#ffd023' }},
            ],[
                { label: "API", href: '/api'}
            ])
        },
        {
            label: "System",
            href: '/system',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Updating Shinobi", href: '/system/update', hubArticle: 'LTVqL3I8f8kIzsX' },
                { label: "Changing the Recording Directory", href: '/system/videos-directory', hubArticle: 'DgvB3tolXOJNm50' },
                { label: "Switching Branches", href: '/configure/branches', hubArticle: 'W5RSTP3R1Jk1RJ5' },
                { label: "Superuser Credentials", href: '/system/superuser', hubArticle: 'pT9DCEudmtjhGrT' },
                { label: "Account Types", href: '/system/accounts', hubArticle: 'yX5mMukjGToZ1HM' },
                { label: "System Logs", href: '/system/logs', hubArticle: 'bp4CWlGcXNHHHAL' },
                // { label: "Configuration", href: '/system/configuration' },
                { label: "Cluster Management", href: '/system/cluster-management', hubArticle: 'c3lWgEAeOtXGcXD' },
                { label: "Language Files", href: '/system/language', hubArticle: 'vpNFA3k0GFwQyEZ' },
                { label: "Enabling HTTPS on Shinobi with Self-Signed Certs", href: '/system/enable-ssl-self-signed', hubArticle: 'u1GwaWRwHD475MK' },
                { label: "Using Google Sign-In", href: '/configure/google-sign-in', hubArticle: 'LBUrlCJEZ1hw0b9' },
                { label: "Custom Modules", href: '/configure/custom-auto-load-modules', hubArticle: '66VZmMzIhHr7mVQ' },
            ],[
                { label: "System", href: '/system'}
            ])
        },
        {
            label: "Remote Management",
            href: '/remote',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Easy Remote Access (P2P)", href: '/configure/p2p', hubArticle: '3Yhivc6djTtuBPE' },
                { label: "Port Forwarding", href: '/remote/port-forward', hubArticle: 'pYUnteHIep5wUS0' },
                { label: "VPN", href: '/remote/vpn', hubArticle: 'pYUnteHIep5wUS0' },
                { label: "OpenVPN Server", href: '/remote/openvpn', hubArticle: 'FeyiRWxe2hd0dht' },
            ],[
                { label: "Remote Management", href: '/remote'}
            ])
        },
        {
            label: "FAQs",
            href: '/faqs',
            itemDraw: true,
            items: addBreadCrumbs([
                { label: "Where is the Shinobi Mobile App?", href: 'https://shinobi.video/mobile', isRemote: true },
                { label: "Updated my Shinobi and now it doesn't work", href: '/faqs/not-working-after-update', hubArticle: '3T1ArCXsUmA1YXy' },
                { label: "My stream is behind? Stream Latency.", href: '/faqs/stream-latency', hubArticle: 'Eug1dxIdhwY6zTw' },
                { label: "How to Insert Videos Externally?", href: '/faqs/video-manual-insert', hubArticle: 'NPdhRHvI1Jf6dRH' },
                { label: "How do I find the Stream URL for my camera or NVR?", href: '/faqs/find-stream-url', hubArticle: 'bS3kYlcCKDF7W7A' },
                { label: "Stop smearing or broken streams", href: '/faqs/smearing-streams', hubArticle: 'PHPllc46JMDm83W' },
                { label: "How I optimized my RTSP camera", href: '/faqs/optimize-rtsp-camera', hubArticle: 'DmWIID78VtvEfnf' },
                { label: "How to add an MJPEG Camera", href: '/faqs/mjpeg-camera-setup', hubArticle: 'jfmP1wFtIMnXlWO' },
                { label: "How to view Shinobi Stream in VLC", href: '/faqs/viewing-shinobi-through-vlc', hubArticle: 'HXbm8gEsaS7oo6l' },
                { label: "How to set a Static IP on Jetson Nano", href: '/faqs/jetson-nano-static-ip', hubArticle: 'Z0kXCFxbQvrHcnm' },
            ],[
                { label: "FAQs", href: '/faqs'}
            ])
        },
    ]
}
