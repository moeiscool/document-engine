pageData = {
    name: "Contributing",
    pageTitle: 'ShinobiDocs : Home > Contributing',
    theme: {
        main: '#305374',
        linkColor: '#76bdff',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: "Search", href: '/search'},
                            ]
                        },
                        {
                           type: "dynamic",
                           dynamic: "h1",
                           class: "text-start section-title-underline pb-4 mb-5",
                           content: 'Contributing',
                        },
                        {
                           type: "paragraph",
                           text: `We greatly appreciate bug reporting and code contributions. You can join <a href="https://shinobi.community" target="_blank">our community chat</a> to coordinate these issues with us quickly. However there are other mediums for reporting that you can go for.`,
                        },
                        {
                           type: "list",
                           list: [
                               {"text": '<a href="https://shinobi.community" target="_blank"><i class="fa fa-comments"></i> Community Chat on Discord</a> - Bug Reporting, Suggestions, Town Square'},
                               {"text": '<a href="https://reddit.com/r/ShinobiCCTV" target="_blank"><i class="fa fa-reddit"></i> Forum on Reddit</a> - SEO Optimized : Bug Reporting, Suggestions'},
                               {"text": '<a href="https://gitlab.com/Shinobi-Systems/Shinobi/-/issues" target="_blank"><i class="fa fa-gitlab"></i> Issues on Gitlab</a> - Bug Reporting'},
                           ],
                        },
                        {
                           type: "paragraph",
                           text: `We respectfully request that you provide changes in the form of a Merge Request to the <a href="https://gitlab.com/Shinobi-Systems/Shinobi/tree/dev" target="_blank">dev branch of the Shinobi repository</a>. All contributed code and materials become the property of Shinobi Systems. Credits for your contributions will remain visible and may be acknowledged on the <a href="https://shinobi.video/heroes" target="_blank">Heroes</a> page.`,
                        },
                        {
                           hyperlink: 'Suggestions / Feature Requests',
                           type: "paragraph",
                           text: `<b>Suggestions / Feature Requests</b>`,
                        },
                        {
                           type: "paragraph",
                           text: `You can post suggestions in the <a href="https://shinobi.community" target="_blank">Discord</a> #suggestions channel but please give careful consideration to your suggestion before posting. The feature may have already been suggested or already planned.`,
                        },
                        {
                           type: "paragraph",
                           text: `Please don't post suggestions in the Issues section. The Issues section is only for bug reporting.`,
                        },
                        {
                           hyperlink: 'Development Requests',
                           type: "paragraph",
                           text: `<b>Development Requests</b>`,
                        },
                        {
                           type: "paragraph",
                           text: `If you wish to have changes made by us (and provided back to the public repository) but don't want to wait too long for our development schedule, you can raise priority for your issue by purchasing any of our products from the <a href="https://licenses.shinobi.video/" target="_blank">ShinobiShop</a>.`,
                        },
                        {
                           type: "paragraph",
                           text: `If you are looking for private changes you can start a private chat with us in the bottom right corner or by contacting a developer in the community chat.`,
                        },
                        {
                           hyperlink: 'Not a Developer?',
                           type: "paragraph",
                           text: `<b>Not a Developer but really want to help?</b>`,
                        },
                        {
                           type: "paragraph",
                           text: `You can greatly support the development by purchasing anything from the <a href="https://licenses.shinobi.video/" target="_blank">ShinobiShop</a>. We suggest going for the <a href="https://licenses.shinobi.video/" href="_blank">Mobile License for Family</a>. It gains extended features on up to 5 devices with our <a href="https://shinobi.video/mobile" target="_blank">Mobile App</a> as well as offering <a href="http://localhost:8001/configure/p2p" target="_blank">Easy Remote Access</a> to view your cameras anywhere in the world (with internet).`,
                        },
                    ]
                },
            ]
        },
    ]
})
