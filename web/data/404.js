pageData = {
    name: "Page Not Found (404)",
    pageTitle: 'Page Not Found (404)',
    thingsYouArePrefix: `This might be`,
    thingsYouAre: `a problem, a miscalculation, an overestimation, an underestimation, a misinterpretation, a slight oversight, a ...`,
    theme: {
        main: '#648a50',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            type: 'section',
            id: 'hero',
            class: 'd-flex flex-column justify-content-center',
            blocks: [
                {
                    type: 'div',
                    class: 'container',
                    style: 'position:relative;z-index:10;',
                    blocks: [
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            content: pageData.name
                        },
                        {
                            type: 'paragraph',
                            text: `${pageData.thingsYouArePrefix || 'I am'} <span class="typed" data-typed-items="${pageData.thingsYouAre}"></span>`,
                        },
                        {
                            type: 'div',
                            class: 'social-links',
                            blocks: [
                                {
                                    type: 'dynamic',
                                    dynamic: 'a',
                                    class: 'home',
                                    attributes: ` href="/"`,
                                    text: `<i class="fa fa-home"></i>`
                                },
                            ]
                        }
                    ]
                },
            ]
        }
    ]
})
