pageData = {
    name: "External Link",
    pageTitle: `External Link : ${href}`,
    theme: {
        main: '#305374',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    }
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            type: 'section',
            id: 'hero',
            class: 'd-flex flex-column justify-content-center',
            blocks: [
                {
                    type: 'div',
                    class: 'container',
                    style: 'position:relative;z-index:10;',
                    blocks: [
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            content: `<a href="${href}" title="Navigate to ${href}"><i class="fa fa-external-link"></i></a> ${pageData.name}`
                        },
                        {
                            type: 'paragraph',
                            text: `You are navigating to an external link. Do you want to continue?`,
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'small',
                            style: 'display:block',
                            content: `<a href="${href}">${href}</a>`,
                        },
                        {
                            type: 'div',
                            class: 'social-links',
                            blocks: [
                                {
                                    type: 'dynamic',
                                    dynamic: 'a',
                                    attributes: ` href="/" title="Home"`,
                                    text: `<i class="fa fa-home"></i>`
                                },
                                {
                                    type: 'dynamic',
                                    dynamic: 'a',
                                    attributes: `href="${href}" title="Continue"`,
                                    text: `<i class="fa fa-external-link"></i>`
                                },
                            ]
                        }
                    ]
                },
            ]
        }
    ]
})
