const renderBlocks = getOldDocLocally(oldDoc);
const prefix = `${renderBlocks[0].breadcrumbs[1].label}`;
const articleTitle = `${renderBlocks[1].content}`;
pageData = {
    name: articleTitle,
    pageTitle: `ShinobiDocs : ${prefix} > ${articleTitle}`,
    theme: theme,
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: renderBlocks
                },
            ]
        },
    ]
})
