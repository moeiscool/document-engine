pageData = {
    name: "Docker",
    pageTitle: 'ShinobiDocs : Home > Search',
    theme: {
        main: '#305374',
        linkColor: '#76bdff',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: "Search", href: '/search'},
                            ]
                        },
                        {
                           type: "dynamic",
                           dynamic: "h1",
                           class: "text-start section-title-underline pb-4 mb-5",
                           content: 'Search Results',
                        },
                        ...searchResults
                    ]
                },
            ]
        },
    ]
})
