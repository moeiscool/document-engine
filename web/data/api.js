const itemBlocks = buildItemDrawRenderBlocks('/api');
itemBlocks.shift();
pageData = {
    name: "Docker",
    pageTitle: 'ShinobiDocs : API',
    theme: {
        main: '#305374',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: "API", href: '/api'},
                            ]
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            class: 'text-start',
                            content: 'RESTful API',
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding',
                            content: '<i class="fa fa-caret-right"></i> Simple and Efficient',
                        },
                        {
                            type: 'paragraph',
                            text: 'Everything that the official dashboard does is through the Shinobi API. Meaning If you decide to build your own user interface or just using certain functions of Shinobi; The API will make quick work of it.',
                        },
                        {
                            type: 'paragraph',
                            text: 'When you authenticate with Shinobi it will offer you an Authorization Token. This token is your Session Key as well and can be used as an API Key. This key will remain active for 15 minutes after the last acivity or while your WebSocket is connected.',
                        },
                        {
                            hyperlink: `Let's Begin`,
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5 pt-3',
                            content: `<i class="fa fa-caret-right"></i> Let's Begin`,
                        },
                        {
                            type: 'paragraph',
                            text: 'Create an API Key or Login to get started.',
                        },
                        {
                            type: 'list',
                            "list":[
                                {
                                    "text":"<a href='/api/authentication'>Authentication</a>",
                                },
                                {
                                    "text":"<a href='/api/managing-api-keys-ui'>Managing API Keys</a>",
                                },
                            ]
                        },
                        {
                            hyperlink: `After Authentication`,
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5 pt-3',
                            content: `<i class="fa fa-caret-right"></i> After Authentication`,
                        },
                        ...itemBlocks
                    ]
                },
            ]
        },
    ]
})
