const itemBlocks = buildItemDrawRenderBlocks(theName)
pageData = {
    name: label,
    pageTitle: `ShinobiDocs : Home > ${label}`,
    theme: {
        main: '#305374',
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: breadcrumbs || []
                        },
                        {
                           type: "dynamic",
                           dynamic: "h1",
                           class: "text-start section-title-underline pb-4 mb-5",
                           content: label,
                        },
                        ...itemBlocks
                    ]
                },
            ]
        },
    ]
})
