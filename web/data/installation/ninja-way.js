pageData = {
    name: "The Ninja Way",
    pageTitle: 'ShinobiDocs : Installation > The Ninja Way',
    theme: {
        main: '#305374',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: "Installation", href: '/installation'},
                                { label: pageData.name },
                            ]
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            class: 'text-start',
                            content: 'The Ninja Way',
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline section-title-underline-less text-start section-title-padding',
                            content: '<i class="fa fa-caret-right"></i> Fast, Easy and Recommended',
                        },
                        {
                            type: 'div',
                            class: 'text-center mb-5',
                            content: `<iframe style='border-radius: 15px' width='560' height='315' src='https://www.youtube.com/embed/pQjTp8TgHn8' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>`,
                        },
                        {
                            hyperlink: 'Installation Warning',
                            type: 'paragraph',
                            text: '<b>Warning :</b> The Ninja Way installs from source code. Which means many libraries from other sources are going to be installed aswell.',
                        },
                        {
                            hyperlink: 'Installer Choices',
                            type: 'paragraph',
                            text: 'The following options are presented when running this for the first time. Some options are community contributions. If you run into issues please contact us in the <a href="https://shinobi.community">community chat</a> or if you have a solution to those problems we welcome you to create a <a href="https://gitlab.com/Shinobi-Systems/Shinobi/-/merge_requests">Merge Request</a> to fix it. 😃',
                        },
                        {
                            type: 'list',
                            list: [
                                {
                                    "text":"Ubuntu - Fast and Touchless"
                                },
                                {
                                    "text":"Ubuntu"
                                },
                                {
                                    "text":"CentOS - Quick Install"
                                },
                                {
                                    "text":"CentOS"
                                },
                                {
                                    "text":"MacOS"
                                },
                                {
                                    "text":"FreeBSD"
                                },
                                {
                                    "text":"OpenSUSE"
                                }
                            ]
                        },
                        {
                            hyperlink: 'Tested Operating Systems',
                            type: 'paragraph',
                            text: 'We have officially tested on the following systems :',
                        },
                        {
                            type: 'list',
                            list: [
                                {
                                    "text":"Ubuntu 22.04"
                                },
                                {
                                    "text":"CentOS 8"
                                },
                                {
                                    "text":"MacOS 10.7"
                                },
                            ]
                        },
                        {
                            hyperlink: `Let's Begin`,
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5 pt-3',
                            content: `<i class="fa fa-caret-right"></i> Let's Begin`,
                        },
                        {
                            type: 'orderList',
                            list:[],
                            blocks: [
                                {
                                    type: 'dynamic',
                                    dynamic: 'li',
                                    blocks: [
                                        {
                                            type: 'paragraph',
                                            "text":"Become <b>root</b> to use the installer and run Shinobi. Use one of the following to do so.",
                                        },
                                        {
                                            type: 'list',
                                            "list":[
                                                {
                                                    "text":"<b>Ubuntu</b> 22.04",
                                                    "code":"sudo su"
                                                },
                                                {
                                                    "text":"<b>CentOS</b> 8",
                                                    "code":"su"
                                                },
                                                {
                                                    "text":"<b>MacOS</b> 10.7",
                                                    "code":"su"
                                                }
                                            ]
                                        },
                                    ]
                                },
                                {
                                    type: 'dynamic',
                                    dynamic: 'li',
                                    blocks: [
                                        {
                                            type: 'paragraph',
                                            "text":"Download and run the installer.",
                                        },
                                        {
                                            type: 'code',
                                           "text":"bash <(curl -s https://gitlab.com/Shinobi-Systems/Shinobi-Installer/raw/master/shinobi-install.sh)"
                                        }
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ]
        },
    ]
})
