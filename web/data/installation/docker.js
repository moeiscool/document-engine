pageData = {
    name: "Docker",
    pageTitle: 'ShinobiDocs : Installation > Docker',
    theme: {
        main: '#0563bb',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: "Installation", href: '/installation'},
                                { label: pageData.name },
                            ]
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            class: 'text-start',
                            content: 'Install Shinobi with Docker',
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline section-title-underline-less text-start section-title-padding',
                            content: '<i class="fa fa-caret-right"></i> Quick and Contained',
                        },
                        {
                            hyperlink: 'Installation Warning',
                            type: 'paragraph',
                            text: '<b>Warning :</b> It is recommended that you have a dedicated machine for Shinobi even if you intend to use Docker. If you are willing to install directly on the operating system please consider installing Ubuntu 22.04 and using the <a href="/installation/ninja-way">Ninja Way</a>.',
                        },
                        {
                            type: 'paragraph',
                            text: `More Information about Docker Installation in <a href='https://gitlab.com/Shinobi-Systems/Shinobi/-/tree/dev/Docker'>the Respository's Docker folder.</a> We also recommend that your Host OS is one of the following :`,
                        },
                        {
                            type: 'list',
                            list: [
                                {
                                    "text":"Ubuntu 22.04"
                                },
                                {
                                    "text":"CentOS 8"
                                },
                                {
                                    "text":"MacOS 10.7"
                                }
                            ]
                        },
                        {
                            hyperlink: `Let's Begin`,
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5 pt-3',
                            content: `<i class="fa fa-caret-right"></i> Let's Begin`,
                        },
                        {
                            type: 'orderList',
                            list:[],
                            blocks: [
                                {
                                    type: 'dynamic',
                                    dynamic: 'li',
                                    blocks: [
                                        {
                                            type: 'paragraph',
                                            "text":"Just do it.",
                                        },
                                        {
                                            type: 'code',
                                           "text":" bash <(curl -s https://gitlab.com/Shinobi-Systems/Shinobi-Installer/raw/master/shinobi-docker.sh)"
                                        }
                                    ]
                                },
                                {
                                    type: 'dynamic',
                                    dynamic: 'li',
                                    content: `Once complete open port 8080 of your Docker host in a web browser.`
                                },
                            ]
                        },
                    ]
                },
            ]
        },
    ]
})
