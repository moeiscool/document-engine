const itemBlocks = buildItemDrawRenderBlocks('/installation');
pageData = {
    name: "Installation",
    pageTitle: 'ShinobiDocs : Installation',
    theme: {
        main: '#305374',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: [
                                { label: "Home", href: '/'},
                                { label: pageData.name },
                            ]
                        },
                        {
                            hyperlink: 'Requirements',
                            type: 'dynamic',
                            dynamic: 'h1',
                            class: 'text-start',
                            content: 'Installation',
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding',
                            content: '<i class="fa fa-caret-right"></i> Requirements',
                        },
                        {
                            type: 'paragraph',
                            text: 'These requirements will be installed for you with <a href="/installation/ninja-way"><b>The Ninja Way</b></a>. The Ninja Way is the recommended way to install. It will allow ease in updating as well as changes to the code.',
                        },
                        {
                            type: 'list',
                            list: [
                                {
                                    text: 'Node.js (16.x)'
                                },
                                {
                                    text: 'FFmpeg (Between 3.3 to 4.1)'
                                },
                                {
                                    text: 'MariaDB (10.4+)'
                                },
                            ]
                        },
                        {
                            type: 'div',
                            content: `<p><b>Node.js (12.x), FFmpeg (Between 3.3 to 4.1), and MariaDB (10.4+)</b> are the main components that Shinobi needs. With the <a href="/installation/ninja-way">Ninja Way</a> these requirements should be fulfilled for you.</p>`,
                        },
                        {
                            hyperlink: 'Recommendation',
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5',
                            content: '<i class="fa fa-caret-right"></i> Recommendation',
                        },
                        {
                            type: 'paragraph',
                            text: `It is recommended you install <b>Ubuntu 22.04</b> on a dedicated machine rather than on <a href="/installation/docker">Docker</a> or a VM.`,
                        },
                        {
                            type: 'list',
                            list: [
                                {
                                    text: 'Dedicated Machine with Intel or AMD CPU (Not VM or Docker)'
                                },
                                {
                                    text: 'Edge Computer like Jetson Nano or Raspberry Pi'
                                }
                            ]
                        },
                        {
                            type: 'paragraph',
                            text: '<b>Note : </b> You can find an old laptop or home computer to use as your server as well as rackable systems.',
                        },
                        {
                            hyperlink: 'Installation Guides',
                            type: 'dynamic',
                            dynamic: 'h4',
                            class: 'section-title-underline text-start section-title-padding mt-5',
                            content: '<i class="fa fa-caret-right"></i> Installation Guides',
                        },
                        ...itemBlocks
                    ]
                },
            ]
        },
    ]
})
