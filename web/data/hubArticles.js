const {
    renderBlocks,
    articleTitle,
    articleOpening,
} = getArticleAsDocument(hubArticle);
pageData = {
    name: articleTitle,
    pageTitle: `ShinobiDocs : ${headerTitle} > ${articleTitle}`,
    theme: theme ? theme : {
        main: '#305374',
        textColorOnMain: '#ffffff',
        hyperlinkColorOnMain: '#ffd023'
    },
};
module.exports = Object.assign(pageData,{
    pageBlocks: [
        {
            id: 'page-top',
            type: 'section',
            class: 'd-flex flex-column justify-content-center',
            hyperlink: 'Top of Page',
            blocks: [
                {
                    type: 'div',
                    blocks: [
                        {
                            type: 'breadcrumbs',
                            breadcrumbs: breadcrumbs || []
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h1',
                            class: 'text-start',
                            content: articleTitle,
                        },
                        {
                            type: 'dynamic',
                            dynamic: 'h6',
                            class: 'section-title-underline section-title-underline-less text-start section-title-padding',
                            content: `${articleOpening}`,
                        },
                        {
                            type: 'div',
                            blocks: renderBlocks
                        },
                    ]
                },
            ]
        },
    ]
})
