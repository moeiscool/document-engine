const pageContents = $('#page-contents')
function scrollto(el){
  let elementPos = $(el).offset().top - 100
  pageContents[0].scrollTo({
    top: elementPos,
    behavior: 'smooth'
  })
}
(function() {
  "use strict";
  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  pageContents.scroll(navbarlinksActive)



  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    pageContents.scroll(toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('body').classList.toggle('mobile-nav-active')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Hero type effect
   */
  const typed = select('.typed')
  if (typed) {
    let typed_strings = typed.getAttribute('data-typed-items')
    typed_strings = typed_strings.split(',')
    new Typed('.typed', {
      strings: typed_strings,
      loop: true,
      typeSpeed: 100,
      backSpeed: 50,
      backDelay: 2000
    });
  }



})()
var mainHeader = $('#top-header')
var searchBar = $('#search-bar')
var searchField = $('#search-field')
var scrollBodies = $('#doc-menu,#page-contents')
var masterMenu = $('#doc-menu')
var masterMenuToggles = $('.toggle-master-menu')
function resetScrollerHeights(){
    var height = mainHeader.height()
    scrollBodies.css('height',`calc(100vh - ${height}px)`)
}

function loadSearchBarValue(){
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    const searchTerm = params.search;
    searchField.val(searchTerm)
}

$(document).ready(() => {
    var pageDrawBody = $('#main')
    $('body').resize(resetScrollerHeights)
    resetScrollerHeights()
    $('#search-submit').click(() => {
        searchBar.submit()
    })
    $('body').on('click','.smooth-href',function(e){
        e.preventDefault()
        var href = $(this).attr('href')
        var nextURL = `${location.origin}${href}`;
        var rawPageLink = `${location.origin}/softload?href=${href}`;
        window.history.pushState(null, null, nextURL);
        $.get(rawPageLink,function(data){
            pageDrawBody.html(data)
        })
        return false;
    })
    $('body').on('click', '.scrollto', function(e) {
        e.preventDefault()
        scrollto($(this).attr('href'))
    })
    searchBar.submit((e) => {
        e.preventDefault()
        var searchTerm = searchField.val()
        window.location.href = "/search?search=" + searchTerm;
        return false;
    })
    loadSearchBarValue()

    masterMenuToggles.click(function(){
        masterMenu.toggleClass('hide-when-small')
    })
})
