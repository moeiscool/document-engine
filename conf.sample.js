module.exports = {
    logoSubText: '',
    pageData: {
        name: "ShinobiDocs",
        pageTitle: 'ShinobiDocs',
        theme: {
            main: '#305374',
            textColorOnMain: '#ffffff',
            hyperlinkColorOnMain: '#ffd023'
        },
    },
    webPort: 8001,
    cacheDefaultPageData: false,
    // tidioKey: 'xxxxxxxxxxxxxxxxxxxxxxx',
    social: [
        { icon: 'home', href: "https://shinobi.video", label: "Home" },
        { icon: 'comments', href: "https://shinobi.community", label: "Community Chat" },
        { icon: 'twitter', href: "https://twitter.com/ShinobiCCTV", label: "Twitter" },
        { icon: 'reddit', href: "https://reddit.com/r/ShinobiCCTV", label: "Reddit" },
        { icon: 'facebook', href: "https://facebook.com/ShinobiCCTV", label: "Facebook" },
        { icon: 'linkedin', href: "https://www.linkedin.com/company/shinobicctv", label: "LinkedIn" },
        { icon: 'instagram', href: "https://www.instagram.com/shinobicctv/", label: "Instagram" },
    ]
}
