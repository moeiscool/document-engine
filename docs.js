process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err)
});
let config
try{
    config = require('./conf.js')
}catch(err){
    console.log('Configuration Failed to Load!')
    return;
}
const { createWebServerApplication } = require('./libs/webServer.js')
const {
    getPageData,
    getPageDataCSS,
    createCacheFolder,
} = require('./libs/utils.js')
const {
    createAppPathsFromMenu,
    createPageBlocksFromSearchResults,
    searchPages,
} = require('./libs/webServerUtils.js')(config)
const {
    masterMenu,
    listOfPagesAsObject,
} = require('./libs/masterMenu.js')
createCacheFolder(config);
const {
    http,
    app,
    io,
} = createWebServerApplication({
    port: config.webPort,
    onClientConnect: (socket) => {

    },
    appPaths: [
        {
            path: '/',
            page: 'pages/index.ejs',
            pageOptions: () => {
                const defaultPageData = getPageData({
                    theName: 'index',
                    useCache: config.cacheDefaultPageData,
                });
                return {
                    config: config,
                    masterMenu: masterMenu,
                    pageData: defaultPageData,
                }
            }
        },
        {
            path: '/assets/css/myProfileColors.css',
            next: (req,res) => {
                const theme = JSON.parse(req.query.theme)
                const themeCSS = getPageDataCSS(theme)
                res.setHeader('content-type', 'text/css')
                res.end(themeCSS)
            },
            pageOptions: {}
        },
        {
            path: '/search',
            page: 'pages/document.ejs',
            pageOptions: (req, res) => {
                const searchResults = searchPages(req.query.search)
                const pageBlocks = createPageBlocksFromSearchResults(searchResults)
                const defaultPageData = getPageData({
                    theName: 'search',
                    useCache: config.cacheDefaultPageData,
                    searchResults: pageBlocks,
                });
                return {
                    config: config,
                    masterMenu: masterMenu,
                    pageData: defaultPageData,
                }
            },
        },
        {
            path: '/softload',
            page: 'pages/raw.ejs',
            pageOptions: (req, res) => {
                const href = req.query.href
                const thePageFramework = getPageData(Object.assign(listOfPagesAsObject[href] || {},{
                    theName: req.query.href,
                    useCache: config.cacheDefaultPageData,
                }));
                return {
                    config: config,
                    masterMenu: masterMenu,
                    pageData: thePageFramework,
                }
            },
        },
        {
            path: '/searchJSON',
            next: (req,res) => {
                const resultsFound = searchPages(req.query.search)
                res.setHeader('content-type', 'application/json')
                res.end(JSON.stringify(resultsFound,null,3))
            },
            pageOptions: {}
        },
        {
            path: '/external',
            page: 'pages/index.ejs',
            pageOptions: (req, res) => {
                const href = req.query.href
                const pageData = getPageData({
                    theName: 'external',
                    useCache: config.cacheDefaultPageData,
                    href,
                });
                return {
                    config,
                    masterMenu,
                    pageData,
                }
            }
        },
        ...createAppPathsFromMenu(masterMenu)
    ],
    defaultPageOptions: {
        config: config,
    }
});
