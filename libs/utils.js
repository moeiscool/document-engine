const config = require('../conf.js');
const {
    buildItemDrawRenderBlocks,
    buildSocialLinksBlocks,
} = require('./html.js')
const {
    getReadmeAsDocument,
} = require('./gitReadme.js')
const {
    downloadArticle,
    writeArticleFile,
    getArticleLocally,
    getArticleAsDocument,
    convertArticleHtmlToJsonDocument,
} = require('./hubArticles.js')
const {
    getOldDocLocally,
} = require('./oldDoc.js')
const fs = require('fs')
const fetch = require('node-fetch')
const mainDirectory = process.cwd()
function createCacheFolder(config){
    const cacheDirectory = `${mainDirectory}/web/data/cached`
    fs.mkdir(cacheDirectory,(err) => {
        if(config.debugLog === true){
            console.log(err)
        }
    })
}

function getPageData({
    theName,
    useCache,
    label,
    oldDoc,
    gitReadme,
    hubArticle,
    headerTitle,
    breadcrumbs,
    itemDraw,
    theme,
    href,
    // for /search
    searchResults
}){
    const filePath = `${mainDirectory}/web/data/${itemDraw ? 'itemDraw' : gitReadme ? 'gitReadme' : hubArticle ? 'hubArticles' : oldDoc ? 'oldDoc' : theName || 'index'}.js`
    let pageData
    try{
        const defaultTheme = {
            main: '#305374',
            textColorOnMain: '#ffffff',
            hyperlinkColorOnMain: '#ffd023'
        }
        theme = theme ? Object.assign(defaultTheme,theme) : defaultTheme
        // if(useCache){
        //     pageData = require(filePath)
        // }else{
            const personFileData = fs.readFileSync(filePath,'utf8').replace('module.exports = ',`pageData = `)
            eval(`try{
                ${personFileData}
            }catch(err){
                console.log(err)
            }`)
        // }
    }catch(err){
        console.log(err)
        console.log(`Failed to Load Page! : ${theName}`)
    }
    if(!pageData){
        pageData = getPageData({
            theName: '404',
            useCache: useCache,
        });
    }
    return pageData
}
function getPageDataCSS(theme){
    const filePath = `${mainDirectory}/web/assets/css/profileColor.css`
    let theSheet = ''
    try{
        theSheet = fs.readFileSync(filePath,'utf8')
            .replace(/LINK_COLOR/g, theme.linkColor || '#76bdff')
            .replace(/TEXT_COLOR_ON_MAIN_COLOR/g, theme.textColorOnMain || '#ffffff')
            .replace(/MAIN_COLOR/g, theme.main || '#305374')
    }catch(err){
        // console.log(err)
        console.log('Default CSS Failed to Load!')
    }
    return theSheet
}

module.exports = {
    getPageData,
    getPageDataCSS,
    createCacheFolder,
}
