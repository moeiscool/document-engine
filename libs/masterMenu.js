const oldRedirects = require('./oldArticleRedirects.json')
let masterMenu
try{
    masterMenu = require('../masterMenu.js')
}catch(err){
    console.log('Loading Sample Menu and Pages')
    masterMenu = require('../masterMenu.sample.js')
}
function getListOfPagesWithData(){
    const list = []
    function processItem(item){
        if(item.isRemote)return;
        if(list.findIndex(item2 => item2.href === item.href) === -1)list.push(item)
    }
    masterMenu.items.forEach(processItem)
    masterMenu.items.forEach((item) => {
        if(item.items)item.items.forEach(processItem)
    })
    return list
}
function getListOfPagesAsObject(){
    const list = {}
    function processItem(item){
        if(item.isRemote)return;
        if(!list[item.href])list[item.href] = item;
    }
    masterMenu.items.forEach(processItem)
    masterMenu.items.forEach((item) => {
        if(item.items)item.items.forEach(processItem)
    })
    return list
}
function getItemsFromHref(href){
    return (masterMenu.items.find((item) => {return item.href === href})).items
}
module.exports = {
    masterMenu,
    oldRedirects,
    getItemsFromHref,
    listOfPagesWithData: getListOfPagesWithData(),
    listOfPagesAsObject: getListOfPagesAsObject(),
}
