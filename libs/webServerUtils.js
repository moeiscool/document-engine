const {
    getPageData,
    searchPages,
} = require('./utils.js')
const {
    masterMenu,
    listOfPagesWithData,
} = require('./masterMenu.js')
module.exports = (config) => {
    function createAppPathsFromMenu(){
        const appPaths = []
        const rawList = []
        function processItem(item){
            if(item.hidden)return;
            if(!item.isRemote){
                console.log(`Adding Path : ${item.href} (${item.label})`)
                appPaths.push({
                    path: item.href,
                    page: 'pages/document.ejs',
                    pageOptions: () => {
                        const thePageFramework = getPageData(Object.assign(item,{
                            theName: item.href,
                            useCache: config.cacheDefaultPageData,
                        }));
                        return {
                            config: config,
                            masterMenu: masterMenu,
                            pageData: thePageFramework,
                        }
                    }
                })
            }
        }
        listOfPagesWithData.forEach(processItem)
        return appPaths
    }

    function scanPageDataForPhrase(page,searchTerm){
        const foundResults = []
        const pageData = page.pageData
        const pageBlocks = pageData.pageBlocks
        function shortenText(text){
            if(!text)return '';
            const textPoint = text.indexOf(searchTerm)
            let startingPoint = textPoint - 20
            startingPoint = startingPoint < 0 ? 0 : startingPoint
            const endingPoint = textPoint + 40
            const strippedHtmlText = text.replace(/(<([^>]+)>)/gi, "")
            const previewText = strippedHtmlText.length > 40 ? (startingPoint > 0 ? '...' : '') + strippedHtmlText.slice(startingPoint, endingPoint).trim() + '...' : strippedHtmlText
            return previewText
        }
        function processItem(item){
            if(foundResults.length === 3)return;
            const text = shortenText(item.text)
            const content = shortenText(item.content)
            const code = shortenText(item.code);
            ([
                text,
                content,
                code,
            ]).forEach((term) => {
                const isFound = term.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
                if(isFound){
                    foundResults.push(term)
                }
            });
            if(item.blocks){
                item.blocks.forEach(processItem)
            }
        }
        pageBlocks.forEach(processItem)
        return foundResults
    }

    function searchPages(searchText){
        if(!searchText)return [];
        const pagesContaining = {}
        const searchTerms = searchText.trim().split(/\s+/);
        const listOfPages = listOfPagesWithData.filter(page => !page.hidden).map((page) => {
            page.pageData = getPageData(Object.assign(page,{
                theName: page.href,
                useCache: config.cacheDefaultPageData,
            }));
            return page
        })
        listOfPages.forEach((page) => {
            let foundSome = false
            searchTerms.forEach((searchTerm) => {
                const previewPhrases = scanPageDataForPhrase(page,searchTerm)
                if(previewPhrases.length > 0){
                    if(!pagesContaining[page.href])pagesContaining[page.href] = {
                        label: page.label,
                        href: page.href,
                        found: {}
                    };
                    pagesContaining[page.href].found[searchTerm] = previewPhrases
                }
            })
        })
        return Object.values(pagesContaining)
    }
    function insertIntoString(str, index, value) {
        return str.substr(0, index) + value + str.substr(index);
    }
    function boldPhraseInString(searchTerm,inString) {
        const lowerString = `${inString}`.toLowerCase()
        const lowerSearchTerm = `${searchTerm}`.toLowerCase()
        const termPoint = lowerString.indexOf(lowerSearchTerm)
        let newString = `${insertIntoString(inString, termPoint, '<b>')}`
        newString = `${insertIntoString(newString, termPoint + searchTerm.length + 3, '</b>')}`
        return newString;
    }
    function createPageBlocksFromSearchResults(searchResults){
        const newBlocks = []
        function buildFoundRows(foundOnPage){
            const newRows = []
            for (const searchTerm in foundOnPage) {
                const list = foundOnPage[searchTerm].map((text) => {
                    return {
                        text: boldPhraseInString(searchTerm,text)
                    }
                });
                newRows.push(...list)
            }
            return newRows
        }
        searchResults.forEach((page) => {
            newBlocks.push({
                type: 'div',
                style: 'margin-bottom: 3rem;',
                blocks: [
                    {
                        hyperlink: page.label,
                        type: 'dynamic',
                        dynamic: 'small',
                        content: `<a href="${page.href}">${page.href}</a>`
                    },
                    {
                        type: 'div',
                        style: 'font-size: 125%;',
                        content: `<a href="${page.href}">${page.label}</a>`
                    },
                    {
                        type: 'list',
                        class: 'text-muted',
                        style: 'list-style: none;padding:0;',
                        list: buildFoundRows(page.found)
                    },
                ]
            })
        })
        return newBlocks
    }
    return {
        createAppPathsFromMenu,
        createPageBlocksFromSearchResults,
        searchPages,
    }
}
