const fs = require('fs')
const mainDirectory = process.cwd()
const articlesDirectory = `${mainDirectory}/web/data/cached`
function getOldDocLocally(articleId){
    try{
        const filePath = `${articlesDirectory}/${articleId}.json`
        const response = JSON.parse(fs.readFileSync(filePath,'utf8'));
        return response;
    }catch(err){
        console.log(err)
    }
}
module.exports = {
    getOldDocLocally,
}
