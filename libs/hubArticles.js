const {
    htmlToRenderBlocks,
    replaceHubArticleUrlsWithNewDocUrls,
} = require('./html.js')
const fs = require('fs')
const fetch = require('node-fetch')
const html2json = require('html2json').html2json;
const mainDirectory = process.cwd()
const articlesDirectory = `${mainDirectory}/web/data/cached`
async function downloadArticle(articleId){
    console.log(`Downloading Article...`,articleId)
    const response = await fetch(`https://hub.shinobi.video/articles/view/${articleId}?json=1`);
    const json = await response.json();
    await writeArticleFile(articleId,json)
    console.log(`Downloaded Article!`,articleId)
    return json
}
async function writeArticleFile(articleId,json){
    console.log(`Writing Article...`,articleId)
    const downloadTo = `${articlesDirectory}/article-${articleId}.json`
    await fs.promises.writeFile(downloadTo,JSON.stringify(json))
    console.log(`Wrote Article!`,articleId)
    return json
}
function getArticleLocally(articleId){
    try{
        const filePath = `${articlesDirectory}/article-${articleId}.json`
        const response = JSON.parse(fs.readFileSync(filePath,'utf8'));
        return response;
    }catch(err){
        // console.log(err)
    }
}
function convertArticleHtmlToJsonDocument(articleInfo){
    const articleId = articleInfo.article.id
    console.log(`Converting Article...`,articleId)
    const articleHtml = articleInfo.article.content
    const renderBlocks = htmlToRenderBlocks(replaceHubArticleUrlsWithNewDocUrls(articleHtml))
    articleInfo.article.content = renderBlocks
    writeArticleFile(articleId,articleInfo).then(() => {
        console.log(`Converted Article!`,articleId)
    }).catch((err) => {
        console.log(`Error Converting Article!`,articleId)
    })
    return {
        article: articleInfo.article,
        articleTitle: articleInfo.title,
        articleOpening: articleInfo.opening,
        renderBlocks,
    }
}
function getArticleAsDocument(articleId){
    const articleInfo = getArticleLocally(articleId)
    if(!articleInfo){
        downloadArticle(articleId).then((data) => {
            console.log(`Done Downloading Article! Refresh Page.`)
        }).catch((err) => {
            console.log(`Failed Downloading Article!`,err)
        })
        return {
            articleTitle: 'Refresh the Page! Article Processing...',
            articleOpening: 'Refresh the Page! Article Processing...',
            renderBlocks: [],
        };
    }
    const contentIsHtml = typeof articleInfo.article.content === 'string';
    if(contentIsHtml){
        return convertArticleHtmlToJsonDocument(articleInfo)
    }
    return {
        article: articleInfo.article,
        articleTitle: articleInfo.title,
        articleOpening: articleInfo.opening,
        renderBlocks: articleInfo.article.content,
    }
}

module.exports = {
    downloadArticle,
    writeArticleFile,
    getArticleLocally,
    getArticleAsDocument,
    convertArticleHtmlToJsonDocument,
}
