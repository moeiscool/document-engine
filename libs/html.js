const html2json = require('html2json').html2json;
const {
    masterMenu,
    oldRedirects,
    getItemsFromHref,
    listOfPagesWithData,
} = require('./masterMenu.js')
function getStringBetween(str, start, end) {
    const result = str.match(new RegExp(start + "(.*)" + end));
    return result[1];
}
function extractTitleFromReadme(articleHtml){
    const rawJsonFromHtml = html2json(articleHtml)
    let titleText = ''
    rawJsonFromHtml.child.forEach((item) => {
        if(item.tag === 'h1' && !titleText){
            titleText = item.child[0].text
        }
    })
    return titleText
}
function replaceHubArticleUrlsWithNewDocUrls(htmlString){
    let newString = `${htmlString}`
    // convert super old links to hubArticle (then convert to newest)
    for (const oldOldArticleName in oldRedirects) {
        const hubArticleId = oldRedirects[oldOldArticleName].id
        newString = newString
            .replace(new RegExp(`https://shinobi.video/articles/${oldOldArticleName}`, 'g'), `https://hub.shinobi.video/articles/view/${hubArticleId}`)
            .replace(new RegExp(`http://shinobi.video/articles/${oldOldArticleName}`, 'g'), `https://hub.shinobi.video/articles/view/${hubArticleId}`);
    }
    // convert hubArticle to newDoc type
    listOfPagesWithData.forEach((item) => {
        const hubArticleId = item.hubArticle;
        const newDocHref = item.hidden || item.href;
        newString = newString
            .replace(new RegExp(`https://hub.shinobi.video/articles/view/${hubArticleId}`, 'g'), `${newDocHref}`)
            .replace(new RegExp(`http://hub.shinobi.video/articles/view/${hubArticleId}`, 'g'), `${newDocHref}`);
        newString = newString
            .replace(new RegExp(`/articles/view/${hubArticleId}`, 'g'), `${newDocHref}`);
    });
    if(newString.indexOf('hub.shinobi.video/articles/view/') > -1){
        console.log(`Page has a link that could not be replaced.`)
        console.log(newString)
    }
    return newString;
}
function getAllTextFromItem(item){
    // var hyperlink = item.child[0].text
    let newText = ``;
    if(item.text)newText += item.text;
    if(item.child){
        function processItem(innerItem){
            if(innerItem.text)newText += innerItem.text;
            if(innerItem.child)innerItem.child.forEach(processItem)
        }
        item.child.forEach(processItem)
    }
    return newText
}
function processHtmlItem(item,parentNodeBlocks,renderBlocks){
    const isText = item.node === 'text';
    const isLineBreak = isText && item.text.replace(/[\r\n]/gm, '').replace(/\s/g,'') === '';
    if(isLineBreak)return;
    let builtItem = null
    let attributes = ``
    const elementNodes = item.child;
    const elementAttr = item.attr;
    for (const key in elementAttr) {
        const value = elementAttr[key];
        attributes += `${key}="${value}" `
    }
    if(attributes === ``){
        attributes = undefined
    }
    if(isText){
        builtItem = {
            type: 'text',
            text: item.text,
        }
    }else{
        switch(item.tag){
            // works for `markdown`
            case'h1':
                var hyperlink = item.child[0].text
                if(renderBlocks.length === 0 && !parentNodeBlocks){

                }else{
                    builtItem = {
                        hyperlink: hyperlink,
                        type: 'dynamic',
                        dynamic: 'h4',
                        class: 'section-title-underline text-start section-title-padding mt-5',
                        content: `<i class="fa fa-caret-right"></i> `,
                        attributes: attributes,
                        blocks: [],
                    }
                }
            break;
            // works for `hubArticle`
            case'h2':
                var hyperlink = getAllTextFromItem(item)
                builtItem = {
                    hyperlink: hyperlink,
                    type: 'dynamic',
                    dynamic: 'h4',
                    class: 'section-title-underline text-start section-title-padding mt-5',
                    content: `<i class="fa fa-caret-right"></i> `,
                    attributes: attributes,
                    blocks: [],
                }
            break;
            case'pre':
                const firstChild = item.child[0]
                const text = firstChild.tag === 'code' ? item.child[0].child[0].text : item.child[0].text
                builtItem = {
                    type: 'code',
                    text: text,
                    attributes: attributes,
                }
            break;
            case'iframe':
                builtItem = {
                    type: 'dynamic',
                    dynamic: 'iframe',
                    style: 'border-radius: 15px;margin-bottom: 3rem;',
                    attributes: attributes,
                    blocks: [],
                }
            break;
            case'table':
                builtItem = {
                    type: 'dynamic',
                    dynamic: 'table',
                    class: 'table table-striped',
                    attributes: attributes,
                    blocks: [],
                }
            break;
            default:
                builtItem = {
                    type: 'dynamic',
                    dynamic: item.tag,
                    attributes: attributes,
                    blocks: [],
                }
            break;
        }
    }
    if(builtItem?.blocks && elementNodes){
        elementNodes.forEach((item) => {
            processHtmlItem(item,builtItem.blocks,renderBlocks)
        })
    };
    if(builtItem)(parentNodeBlocks || renderBlocks).push(builtItem);
}
function htmlToRenderBlocks(articleHtml){
    const renderBlocks = []
    try{
        const articleJson = html2json(articleHtml);
        articleJson.child.forEach((item) => {
            processHtmlItem(item,null,renderBlocks)
        })
    }catch(err){
        console.log(err)
    }
    return renderBlocks
}
function buildItemDrawRenderBlocks(href){
    const newBlocks = []
    const items = getItemsFromHref(href);
    items.forEach((item) => {
        newBlocks.push({
            type: 'div',
            style: 'margin-bottom: 3rem;',
            blocks: [
                {
                    hyperlink: item.label,
                    type: 'div',
                    style: 'font-size: 125%;',
                    content: `<i class="fa fa-square-o"></i> &nbsp; <a href="${item.href}">${item.label}</a>`
                },
                {
                    type: 'div',
                    style: 'cursor: default;',
                    content: item.description || `<span class="text-muted">${item.href}</span>`
                },
            ]
        })
    })
    return newBlocks
}
function buildSocialLinksBlocks(socialLinks){
    const newBlock = {
        type: 'div',
        class: 'social-links',
        blocks: socialLinks.map((item) => {
            return {
                type: 'dynamic',
                dynamic: 'a',
                class: 'home',
                attributes: ` href="${item.href}" title="${item.label}"`,
                text: `<i class="fa fa-${item.icon}"></i>`
            }
        })
    }
    return newBlock
}
module.exports = {
    processHtmlItem,
    getStringBetween,
    htmlToRenderBlocks,
    extractTitleFromReadme,
    buildItemDrawRenderBlocks,
    replaceHubArticleUrlsWithNewDocUrls,
    buildSocialLinksBlocks,
}
