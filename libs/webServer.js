const fs = require('fs');
const express = require('express');
const url = require('url');
const bodyParser = require('body-parser');
module.exports = {
    createWebServerApplication: (options) => {
        options = options ? options : {}
        options.port = options.port || 3001
        var app = express();
        var http = require('http').createServer(app);
        var io = require('socket.io')(http);
        io.engine.ws = new (require('cws').Server)({
            noServer: true,
            perMessageDeflate: false
        })
        io.on('connection', (socket) => {
            if(options.onClientConnect){
                options.onClientConnect(socket)
            }
            if(options.onClientEvent){
                Object.keys(options.onClientEvent).forEach((key) => {
                    socket.on(key,options.onClientEvent[key])
                })
            }
            // var lastPingTime
            // var currentMs = 0
            // var startHeartBeat = setInterval(function(){
            //     lastPingTime = new Date()
            //     socket.emit('heartbeat',{})
            // },5000)
            // socket.on('heartbeat',() => {
            //     var ms = (new Date()) - lastPingTime;
            //     socket.emit('ms',ms)
            // })
            // socket.on('disconnect',() => {
            //     clearInterval(startHeartBeat)
            // })
        })

        app.set('views', process.cwd() + '/web')

        app.set('view engine','ejs')

        app.use('/', express.static(process.cwd() + '/web'))

        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());

        if(options.appPaths){
            options.appPaths.forEach((pathOptions) => {
                app[pathOptions.type ? pathOptions.type : 'get'](pathOptions.path, (req, res) => {
                    let pageOptions = {}
                    if(typeof pathOptions.pageOptions === 'function'){
                        pageOptions = pathOptions.pageOptions(req, res)
                    }else{
                        pageOptions = pathOptions.pageOptions || options.defaultPageOptions || {}
                    }
                    if(pathOptions.next){
                        pathOptions.next(req,res,pageOptions)
                    }
                    if(pathOptions.page){
                        res.render(pathOptions.page,pageOptions)
                    }
                })
            })
        }

        http.on('upgrade', function upgrade(request, socket, head) {
            const pathname = url.parse(request.url).pathname;
            // if (pathname === '/videoStream') {
            //     wss.handleUpgrade(request, socket, head, function done(ws) {
            //         wss.emit('connection', ws, request);
            //     });
            // } else
            if (pathname.indexOf('/socket.io') > -1) {
                // leave it blank because i dunno, it makes socket.io work.
                return;
            } else {
                socket.destroy();
            }
        })

        http.listen(options.port, () => {
            console.log('listening on *:' + options.port);
        })

        return {
            http: http,
            app: app,
            io: io,
        }
    },
    getIpAddress: (req) => {
        return req.headers['cf-connecting-ip'] ||
            req.headers["CF-Connecting-IP"] ||
            req.headers["'x-forwarded-for"] ||
            req.connection.remoteAddress
    }
}
