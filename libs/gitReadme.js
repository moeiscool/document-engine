const {
    extractTitleFromReadme,
    htmlToRenderBlocks,
    getStringBetween,
    processHtmlItem,
} = require('./html.js')
const fs = require('fs')
const fetch = require('node-fetch')
const markdownToHtml = require('markdown-it')();
const mainDirectory = process.cwd()
const html2json = require('html2json').html2json;
const readmeDirectory = `${mainDirectory}/web/data/cached`
async function downloadReadme(readmeId){
    console.log(`Downloading README...`,readmeId)
    function buildLink(prefix){
        return `https://gitlab.com/Shinobi-Systems/Shinobi/-/${prefix}/dev/${readmeId}`
    }
    const readmeRepo = buildLink('raw')
    const response = await fetch(`${readmeRepo}/README.md`);
    const markdownBody = (await response.text())
        .replace(/\*\*.*?\*\*/g, function(string) {
            const innterString = string.substring(2, string.length-2).trim()
            console.log('innterString',innterString)
            return `**${innterString}**`;
        });
    const articleHtml = markdownToHtml.render(markdownBody)
    const articleContent = html2json(articleHtml)
    const authorId = "gitlab:Shinobi-Systems/Shinobi"
    const dateNow = new Date()
    const articleTitle = extractTitleFromReadme(articleHtml)
    const articleTreeLink = buildLink('tree')
    const articleOpening = `<a href="${articleTreeLink}"><i class="fa fa-external-link"></i> Go to Repository</a>`
    const json = {
       "pageTitle": "ShinobiHub - Article : How to use Motion Detection (Dashboard V3)",
       "author": {
          "id": authorId,
          "name": "moeiscool",
          "details": {
             "fname": "Moe",
             "lname": "Alam"
          }
       },
       "article": {
          "id": safeReadmeId(readmeId),
          "uploaderId": authorId,
          "content": articleHtml,
          "dateAdded": dateNow,
          "dateUpdated": dateNow,
       },
       "title": articleTitle,
       "opening": articleOpening
   };
    await writeReadmeFile(readmeId,json)
    console.log(`Downloaded README!`,readmeId)
    return json
}
function safeReadmeId(readmeId){
    return readmeId.replace(/\//g,'-')
}
function getReadmeFilePath(readmeId){
    return `${readmeDirectory}/readme-${safeReadmeId(readmeId)}.json`
}
async function writeReadmeFile(readmeId,json){
    console.log(`Writing README...`,readmeId)
    const downloadTo = getReadmeFilePath(readmeId)
    await fs.promises.writeFile(downloadTo,JSON.stringify(json))
    console.log(`Wrote README!`,readmeId)
    return json
}
function getReadmeLocally(readmeId){
    try{
        const filePath = getReadmeFilePath(readmeId)
        const response = JSON.parse(fs.readFileSync(filePath,'utf8'));
        return response;
    }catch(err){
        // console.log(err)
    }
}
function convertReadmeHtmlToJsonDocument(readmeInfo){
    const readmeId = readmeInfo.article.id
    console.log(`Converting README...`,readmeId)
    const articleHtml = readmeInfo.article.content
    const renderBlocks = htmlToRenderBlocks(articleHtml)
    readmeInfo.article.content = renderBlocks
    writeReadmeFile(readmeId,readmeInfo).then(() => {
        console.log(`Converted README!`,readmeId)
    }).catch((err) => {
        console.log(`Error Converting README!`,readmeId)
    })
    return {
        article: readmeInfo.article,
        articleTitle: readmeInfo.title,
        articleOpening: '',
        renderBlocks,
    }
}
function getReadmeAsDocument(readmeId){
    const readmeInfo = getReadmeLocally(readmeId)
    if(!readmeInfo){
        downloadReadme(readmeId).then((data) => {
            console.log(`Done Downloading Readme! Refresh Page.`)
        }).catch((err) => {
            console.log(`Failed Downloading Readme!`,err)
        })
        return {
            articleTitle: 'Refresh the Page! Readme Processing...',
            articleOpening: 'Refresh the Page! Readme Processing...',
            renderBlocks: [],
        };
    }
    const contentIsHtml = typeof readmeInfo.article.content === 'string';
    if(contentIsHtml){
        return convertReadmeHtmlToJsonDocument(readmeInfo)
    }
    return {
        article: readmeInfo.article,
        articleTitle: readmeInfo.title,
        articleOpening: readmeInfo.opening,
        renderBlocks: readmeInfo.article.content,
    }
}

module.exports = {
    downloadReadme,
    safeReadmeId,
    getReadmeFilePath,
    writeReadmeFile,
    getReadmeLocally,
    getReadmeAsDocument,
    convertReadmeHtmlToJsonDocument,
}
