const fs = require('fs')
const {
    masterMenu,
    listOfPagesWithData,
} = require('../libs/masterMenu.js')
const oldOldRedirects = require('../libs/oldArticleRedirects.json')
const newlyCreatedFilePath = __dirname + '/oldArticleRedirects.json'
const newRedirects = {}

for (const oldPath in oldOldRedirects) {
    const hubArticleId = oldOldRedirects[oldPath].id
    const docRow = listOfPagesWithData.find(row => row.hubArticle === hubArticleId)
    const newLink = docRow ? docRow.href : undefined
    newRedirects[oldPath] = {
        id: hubArticleId,
        href: newLink,
    }
}

fs.writeFileSync(newlyCreatedFilePath,JSON.stringify(newRedirects,null,3))
