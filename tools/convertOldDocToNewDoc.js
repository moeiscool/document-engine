const fs = require('fs')
const fetch = require('node-fetch')
const mainDirectory = process.cwd()
const articlesDirectory = `${mainDirectory}/web/data/articles`
const dataFilename = process.argv[2] || 'api'
console.log(`Articles Directory : "${articlesDirectory}"...`)
console.log(`Processing for "${dataFilename}"...`)
async function downloadDoc(docId){
    console.log(`Downloading Doc...`,docId)
    const response = await fetch(`https://gitlab.com/Shinobi-Systems/shinobi.video/-/raw/master/web/data/${docId}.json`);
    const json = await response.json();
    console.log(`Downloaded Doc!`,docId)
    return json
}
async function writeDocFile(docId,json){
    console.log(`Writing Doc...`,docId)
    const downloadTo = `${articlesDirectory}/${docId}.json`
    await fs.promises.writeFile(downloadTo,JSON.stringify(json,null,3))
    console.log(`Wrote Doc!`,docId)
    return json
}
function convertOldDocToNewDoc(json){
    function processSection(item){
        const newBlocks = []
        function pushToArrays(dataBlock){
            newBlocks.push(dataBlock);
        }
        function processItem(item,parentBlocks){
            function pushToInner(dataBlock){
                (parentBlocks || newBlocks).push(dataBlock);
            }
            if(item.text){
                let text = item.text
                if(text.startsWith('<b>')){
                    const textParts = text.split('</b>')
                    let newText = textParts[0].replace('<b>','')
                    const newInfo = textParts[1];
                    if(newText.endsWith('.'))newText = newText.slice(0, -1)
                    pushToInner({
                        hyperlink: newText,
                        type: 'dynamic',
                        dynamic: 'h6',
                        class: 'item-title',
                        content: newText,
                    });
                    pushToInner({
                        type: 'paragraph',
                        text: newInfo,
                    });
                }else{
                    let newText = `${item.text}`
                    if(newText.endsWith('.'))newText = newText.slice(0, -1)
                    pushToInner({
                        hyperlink: newText,
                        type: 'dynamic',
                        dynamic: 'h6',
                        class: newText.length > 40 ? '' : 'item-title',
                        content: newText,
                    })
                }
            }
            if(item.info){
                pushToInner({
                    type: 'paragraph',
                    text: item.info,
                })
            }
            if(item.code || item.json){
                pushToInner({
                    type: 'code',
                    text: item.code || JSON.stringify(item.json,null,3),
                })
            }
            if(item.list){
                pushToInner({
                    type: 'list',
                    list: item.list.map(listItem => {
                        return {
                            "text": listItem
                        }
                    })
                })
            }
            if(item.blocks){
                const newDiv = {
                    type: 'div',
                    style: 'padding-left: 2rem;',
                    blocks: []
                }
                item.blocks.forEach((item2) => {
                    processItem(item2, newDiv.blocks)
                });
                pushToArrays(newDiv);
            }
            return newBlocks
        }
        if(item.description){
            const sectionTitle = item.description
            pushToArrays(
                {
                    type: 'breadcrumbs',
                    breadcrumbs: [
                        { label: "Home", href: '/'},
                        { label: "API", href: '/api'},
                        { label: sectionTitle },
                    ]
                }
            );
            pushToArrays(
                {
                    type: 'dynamic',
                    dynamic: 'h1',
                    class: 'text-start section-title-underline pb-4 mb-5',
                    content: sectionTitle,
                },
                // {
                //     hyperlink: sectionTitle,
                //     type: 'dynamic',
                //     dynamic: 'h4',
                //     class: 'section-title-underline text-start section-title-padding mt-5',
                //     content: `<i class="fa fa-caret-right"></i> ${sectionTitle}`,
                // }
            );
        }
        if(item.blocks){
            item.blocks.forEach((item2) => {
                processItem(item2)
            });
        }
        const cleanName = item.description.toLowerCase().replace(/[^A-Z0-9]+/ig, "-").split('-').filter(piece => !!piece).join('-')
        const newName = `${dataFilename}-${cleanName}`
        console.log(`Saving "${newName}"!`)
        writeDocFile(newName, newBlocks)
        return newBlocks
    }
    json.forEach((item) => {
        processSection(item)
    })
}
async function run(){
    const oldDoc = await downloadDoc(dataFilename)
    const newDoc = convertOldDocToNewDoc(oldDoc)

}
run().then((data) => {
    console.log(`Done Processing "${dataFilename}"!`)
}).catch((err) => {
    console.log(err)
})
