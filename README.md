# ShinobiDocs

This is the official repository for the Shinobi Systems Document engine.

You may notice that much of the document information isn't here in this repository. This engine generates most documents from :

- Articles from ShinobiHub by Article ID
- READMEs from the Shinobi repository by filename
- Old Shinobi Documents with an included tool to convert to new document type

Information from other sources may also be generated or hardcoded as well.

# The Cream

See `masterMenu.js`. This file is the basis for the entire engine. By declaring an Article ID, README handle, or pre-converted doc it will load them automatically.

https://gitlab.com/Shinobi-Systems/document-engine/-/blob/main/masterMenu.js

# Issues

Please post all issues about the document engine here. Suggestions, Bug Reports, and Questions. These issues should **not** be used for any other Shinobi Systems' product.
You may also raise these issues in the community chat but they are preferred in the issues section for the document engine.

- Issues : https://gitlab.com/Shinobi-Systems/document-engine/-/issues
- Community Chat : https://shinobi.community/

# Contributing

You are welcome to make Merge Requests to the Document Engine core. Some articles are only modifiable on **ShinobiHub** by the author or an administrator.

- Merge Request : https://gitlab.com/Shinobi-Systems/document-engine/-/merge_requests
- ShinobiHub : https://hub.shinobi.video/articles

# External Use

This engine is free to fork and use for your purposes but you need to link back to this repository in your footer.

```
<a href="https://gitlab.com/Shinobi-Systems/document-engine">Document Engine created by Shinobi Systems</a>
```
